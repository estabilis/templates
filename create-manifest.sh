#!/bin/bash
cat <<EOF >> k8s-manifest.yml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: ${CI_ENVIRONMENT_NAME}-${CI_PROJECT_ROOT_NAMESPACE}-${CI_PROJECT_NAME}
  name: ${CI_PROJECT_NAME}
  labels:
    app: ${CI_PROJECT_NAME}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ${CI_PROJECT_NAME}
  template:
    metadata:
      labels:
        app: ${CI_PROJECT_NAME}
    spec:
      containers:
      - name: ${CI_PROJECT_NAME}
        image: ${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_SHORT_SHA}
        imagePullPolicy: Always
      imagePullSecrets:
        - name: ${CI_PROJECT_NAME}
---
apiVersion: v1
kind: Service
metadata:
  namespace: ${CI_ENVIRONMENT_NAME}-${CI_PROJECT_ROOT_NAMESPACE}-${CI_PROJECT_NAME}
  name: ${CI_PROJECT_NAME}-svc
  labels:
    app: ${CI_PROJECT_NAME}
spec:
  ports:
  - port: ${TARGET_PORT}
    targetPort: ${TARGET_PORT}
    name: http
  selector:
    app: ${CI_PROJECT_NAME}
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  namespace: ${CI_ENVIRONMENT_NAME}-${CI_PROJECT_ROOT_NAMESPACE}-${CI_PROJECT_NAME}
  name: ${CI_PROJECT_NAME}-ingress
  annotations:
      cert-manager.io/cluster-issuer: acme
      acme.cert-manager.io/http01-edit-in-place: "true" 
spec:
  tls:
  - hosts:
    - ${CI_PROJECT_PATH_SLUG}.${CI_ENVIRONMENT_URL}
    secretName: ${CI_PROJECT_NAME}-secret
  ingressClassName: nginx
  rules:
   - host: ${CI_PROJECT_PATH_SLUG}.${CI_ENVIRONMENT_URL}
     http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: ${CI_PROJECT_NAME}-svc
                port:
                  name: http
---
EOF
